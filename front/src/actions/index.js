import { SET_QUERY, SET_PAGE, SET_RESULTS, SET_NUMBEROFRESULTS, SET_OVERDUE, SET_MOSTDRAW } from "../constants/action-types";

export const setQuery = query => ({
  type: SET_QUERY,
  payload: query
});

export const setPage = number => ({
  type: SET_PAGE,
  payload: number
});

export const setResults = results => ({
  type: SET_RESULTS,
  payload: results
});

export const setNumberOfResults = total => ({
  type: SET_NUMBEROFRESULTS,
  payload: total
});

export const setOverdue = results => ({
  type: SET_OVERDUE,
  payload: results
});

export const setMostDraw = results => ({
  type: SET_MOSTDRAW,
  payload: results
});