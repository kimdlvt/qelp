export const SET_QUERY    = "SET_QUERY";
export const SET_PAGE  	  = "SET_PAGE";
export const SET_RESULTS  = "SET_RESULTS";
export const SET_NUMBEROFRESULTS  = "SET_NUMBEROFRESULTS";
export const SET_OVERDUE  = "SET_OVERDUE";
export const SET_MOSTDRAW = "SET_MOSTDRAW";

