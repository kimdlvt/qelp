import React, {Component} from 'react';

export default class Title extends Component {

  render() {
    return (
      <div className="container-fluid bg-light border-top border-bottom">
        <h1 className="p-3 font-weight-light text-left ml-4">
          {( (this.props.title !== '') ? 'Concurso ' + this.props.title : "Lista de Resultados")}
        </h1>
      </div>
    )
  }
}