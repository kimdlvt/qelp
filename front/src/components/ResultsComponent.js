import React, {Component} from 'react';


export default class ResultsComponent extends Component {

  render() {
    //console.log(this.props.results);
    if (this.props.results.length < 1 ) {
    return (
      <div className="container mb-4 pt-4">
        <p>Nenhum resultado encontrado</p>
      </div>
    );
  } else {
    return (
      <div className="container mb-4 mt-2">
        <div className="card" key="header">
          <div className="card-body row p-1">
            <div className="col border-right">
              <h5 className="card-text pt-3">Concurso</h5>
            </div>
            <div className="col border-right">
              <h5 className="card-text pt-3">Data</h5>
            </div>
            <div className="col">
              <h5 className="card-text pt-3">Dezenas</h5>
            </div>
          </div>
        </div>
      {this.props.results.map(resultado => {
          return (
            <div className="card" key={resultado._id}>
              <div className="card-body row p-1">
                <div className="col border-right">
                  <h5 className="card-text pt-3">{resultado.Concurso}</h5>
                </div>
                <div className="col border-right">
                  <h5 className="card-text pt-3">{resultado.Data}</h5>
                </div>
                <div className="col">
                  <h5 className="card-text pt-3"><span className="px-2">{resultado.bola1}</span>
                                                 <span className="px-2">{resultado.bola2}</span>
                                                 <span className="px-2">{resultado.bola3}</span>
                                                 <span className="px-2">{resultado.bola4}</span> 
                                                 <span className="px-2">{resultado.bola5}</span> 
                                                 <span className="px-2">{resultado.bola6}</span> 
                  </h5>
                </div>
              </div>
            </div>
         )}      
   )}     
    </div>

    )
  }}
}