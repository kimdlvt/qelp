import React, {Component} from 'react';
import ResultsComponent from '../components/ResultsComponent';
import TriviaComponent from '../components/TriviaComponent';

export default class ContentComponent extends Component {

  render() {
     return (
      <div className="container">
        <div className="row">
          <div className="col-9"><ResultsComponent results={this.props.results} /></div>
          <div className="col-3"><TriviaComponent mostDraw={this.props.mostDraw} overdue={this.props.overdue} /></div>
        </div>
      </div>
    )   
  }
}