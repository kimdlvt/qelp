import React, {Component} from 'react';
import Pagination from "react-js-pagination";

export default class FooterComponent extends Component {

  constructor(props) {
      super(props);

      this.handlePageChange = this.handlePageChange.bind(this);
  }

  handlePageChange(pageNumber) {
    this.props.callbackPage(pageNumber);
  }

  /*handlePrevious = evt => {
    evt.preventDefault();
    this.props.callbackPage(this.props.currentPage - 1);
  }

  handleNext = evt => {
    evt.preventDefault();
    this.props.callbackPage(this.props.currentPage + 1);
  }*/

  render() {
    return (
      <div className="container">
        <hr />
          <div className="float-right">
            <Pagination
                activePage={this.props.currentPage}
                itemsCountPerPage={this.props.paginationSize}
                totalItemsCount={this.props.numberOfResults}
                pageRangeDisplayed={6}
                onChange={this.handlePageChange}                
                itemClass={"page-item"}
                linkClass={"page-link"}
            />
        </div>
      </div>
    )
  }
}