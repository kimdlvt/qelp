import React, {Component} from 'react';

export default class SearchComponent extends Component {

  constructor(props) {
    super(props);
    this.state = {
      query: ''
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({query: event.target.value});
    }

  handleSubmit(event) {
      this.props.callbackQuery(this.state.query);
      event.preventDefault();
  }

  render() {
    return (
    <nav className="navbar navbar-expand-lg navbar-light bg-white">
    <a className="navbar-brand  pl-4" href="#"><img src="https://logodownload.org/wp-content/uploads/2018/10/mega-sena-logo-5.png" alt="logo" /></a>
      <form className="form-inline my-2 my-lg-0 ml-auto" onSubmit={this.handleSubmit}>
        <input className="form-control mr-sm-2"
          placeholder="Pesquisar por concurso"
          value={this.state.query} onChange={this.handleChange}
        />

      </form>
      </nav>
    )
  }
}