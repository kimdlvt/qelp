import React, {Component} from 'react';


export default class TriviaComponent extends Component {

  render() {

    return (
      <div>
        <div className="container mb-4 mt-2">
          <div className="card" key="header">
          <div className="card-body row p-1"><div className="col">Dezenas mais sorteadas</div></div>
            <div className="card-body row p-1">
              <div className="col border-right">
                <h6 className="card-text pt-2">Dezena</h6>
              </div>
              <div className="col">
                <h6 className="card-text pt-2">Sorteios</h6>
              </div>
            </div>
          </div>
        {this.props.mostDraw.map(dezena => {
          console.log(dezena)
            return (
              <div className="card" key={dezena._id}>
                <div className="card-body row p-1">
                  <div className="col border-right">
                    <h6 className="card-text pt-2">{dezena.dezena}</h6>
                  </div>
                  <div className="col">
                    <h6 className="card-text pt-2">{dezena.vezes}</h6>
                  </div>
                </div>
              </div>
           )}      
     )}     
      </div>
      <div className="container mb-4 mt-2">
          <div className="card" key="header">
          <div className="card-body row p-1"><div className="col">Dezenas atrasadas</div></div>
            <div className="card-body row p-1">
              <div className="col border-right">
                <h6 className="card-text pt-2">Dezena</h6>
              </div>
              <div className="col">
                <h6 className="card-text pt-2">Sorteios sem sair</h6>
              </div>
            </div>
          </div>
        {this.props.overdue.map(dezena => {
          console.log(dezena)
            return (
              <div className="card" key={dezena._id}>
                <div className="card-body row p-1">
                  <div className="col border-right">
                    <h6 className="card-text pt-2">{dezena.dezena}</h6>
                  </div>
                  <div className="col">
                    <h6 className="card-text pt-2">{dezena.sorteios}</h6>
                  </div>
                </div>
              </div>
           )}      
     )}     
      </div>
    </div>
    )

  }
}