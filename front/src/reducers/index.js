import { SET_QUERY, SET_PAGE, SET_RESULTS, SET_NUMBEROFRESULTS, SET_OVERDUE, SET_MOSTDRAW } from "../constants/action-types";

export default (state, action) => {
  switch (action.type) {

    case SET_QUERY:
      return {
        ...state,
        query: action.payload
      };

      case SET_PAGE:
      return {
        ...state,
        currentPage: action.payload
      };

      case SET_RESULTS:
      return {
        ...state,
        results: action.payload
      };

      case SET_NUMBEROFRESULTS:
      return {
        ...state,
        numberOfResults: action.payload
      };

      case SET_OVERDUE:
      return {
        ...state,
        overdue: action.payload
      };

      case SET_MOSTDRAW:
      return {
        ...state,
        mostDraw: action.payload
      };

    default:
      return state;
  }
};