import { createStore } from "redux";
import reducer from "../reducers";

const initialState = {
    currentPage: 1,
    numberOfResults: 0,
    paginationSize: 20,
    query: '',
    results: [],
    overdue: [],
    mostDraw: []
  };

export const store = createStore(reducer, initialState);