import React, { Component } from 'react';
import { searchResults, getMostDraw, getOverdue } from '../api-client';
import { createStore } from "redux";
import './App.css';
import './Pagination.css';

import SearchComponent from '../components/SearchComponent';
import TitleComponent from '../components/TitleComponent';
import ContentComponent from '../components/ContentComponent';
import FooterComponent from '../components/FooterComponent';

import reducer from "../reducers";
import { store } from "../store";
import { setQuery, setPage, setResults, setNumberOfResults, setOverdue, setMostDraw } from "../actions";

class App extends Component {

  getInfo = () => {

    searchResults(
      store.getState().currentPage, 
      store.getState().query
    ).then(({ data }) => {
        store.dispatch(setNumberOfResults(data.total));
        store.dispatch(setResults(data.results));
      })
  }

  getTrivia = () => {

    getMostDraw().then(({ data }) => {
        store.dispatch(setMostDraw(data));
      });

    getOverdue().then(({ data }) => {
        store.dispatch(setOverdue(data));
      })

  }

  componentDidMount(){
     this.getInfo();
     this.getTrivia();
  }

  queryCallback = (newQuery) => {
    store.dispatch(setQuery(newQuery));
    this.getInfo()
  }

  pageCallback = (newPage) => {
    store.dispatch(setPage(newPage));
    this.getInfo()
  }

  render() {
    return (
      <div className="App">
        <SearchComponent  callbackQuery={this.queryCallback} />
        <TitleComponent   title={store.getState().query} />
        <ContentComponent results={store.getState().results} mostDraw={store.getState().mostDraw} overdue={store.getState().overdue}   />
        <FooterComponent  paginationSize={store.getState().paginationSize} numberOfResults={store.getState().numberOfResults} 
                          currentPage={store.getState().currentPage} callbackPage={this.pageCallback} />
      </div>
    );
  }
}


export default App;