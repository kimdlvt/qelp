import axios from 'axios';
const API_URL = 'http://127.0.0.1:8000/resultado/';


export const searchResults = ( page, query ) => {
  
  var url = `${API_URL}page/${page}/`;

  if(query !== '') { url += `search/${query}`; }

  return axios.get(url);

};


export const getMostDraw = () => {
  
  var url = `${API_URL}mostDraw/`;

  return axios.get(url);

};


export const getOverdue = () => {
  
  var url = `${API_URL}overdue/`;

  return axios.get(url);

};

