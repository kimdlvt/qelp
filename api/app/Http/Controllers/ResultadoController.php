<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Resultado;
use App\Atrasada;
use App\Sorteada;

class ResultadoController extends Controller
{

    public function index($page = 1)
    {
    	$start = ($page-1) * 20;

        $total   = Resultado::count();
        $results = Resultado::orderBy('Concurso', 'desc')->skip($start)->take(20)->get();

        $response = array('total'=> $total, 'results' => $results);

        return json_encode($response);
    }

    public function search($page = 1, $concurso)
    {
        $concurso = (int) $concurso;
    	$start = ($page-1) * 20;

        $total = Resultado::where('Concurso', $concurso)->count();
        $results = Resultado::where('Concurso', $concurso)->orderBy('order', 'desc')->skip($start)->take(20)->get();

        $response = array('total'=> $total, 'results' => $results);

        return json_encode($response);
    }


    public function mostDraw()
    {
        $results = Sorteada::orderBy('vezes', 'desc')->get();

        return $results;
    }

    public function overdueDraw()
    {
        $results = Atrasada::orderBy('sorteios', 'desc')->get();

        return $results;
    }

}
