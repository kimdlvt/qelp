<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Sorteada extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'sorteadas';
}
