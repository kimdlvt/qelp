<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});
*/
Route::get('resultado/page/{page}','ResultadoController@index');

Route::get('resultado/page/{page}/search/{number}','ResultadoController@search');

Route::get('resultado/mostDraw','ResultadoController@mostDraw');

Route::get('resultado/overdue','ResultadoController@overdueDraw');